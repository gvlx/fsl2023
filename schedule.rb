#!/usr/bin/env ruby

require 'yaml'
require 'erb'
require 'kramdown'

class String
  def to_html
    Kramdown::Document.new(self).to_html.gsub(/>\s+</, "><")
  end
end

def slug(text)
  text.downcase.gsub(" ", "-")
end

schedule = YAML.load(File.read("schedule.yaml"))

schedule["tracks"].each do |track|
  track["schedule"].each do |talk|
    talk["description"] = talk["description"]&.to_html
    talk["id"] = "#{slug(track["room"])}-#{track["day"]}-#{talk["from"]}"
  end
end

$stderr.puts schedule["tracks"].map { |t| t["schedule"].size }.sum

def rowspan(ts, start, finish)
  ts
    .drop_while {|(h,m)| (h * 3600 + m * 60) < start }
    .take_while { |(h,m)| (h * 3600 + m * 60) < finish }
    .length
end

def valid_times(schedule, day)
  hours = [9,10,11,12,13,14,15,16,17,18,19,20]
  minutes = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]

  hours.product(minutes)
    .select do |(hour, minute)|
      seconds = (hour * 60 + minute) * 60

      (day < 17 && seconds < 20.5 * 3600) ||
      (day == 17 && seconds < 18.5 * 3600)
    end
    .select do |(hour, minute)|
      seconds = (hour * 60 + minute) * 60

      seconds == 12*3600 ||
      seconds == 14*3600 ||
      seconds == 19*3600 ||
      seconds == 20.5*3600 ||
      minute == 0 || minute == 15 || minute == 30 || minute == 45 ||
        schedule["tracks"]
        .filter { |track| track["day"] == day }
        .flat_map { |track| track["schedule"] }
        .any? { |talk| talk["from"] == seconds || talk["to"] == seconds }
  end
end

def organizer(schedule, day, room, from, to)
  schedule["tracks"].find do |t|
    t["day"] == day &&
    t["room"] == room &&
    t["schedule"].any? { |talk| from*3600 <= talk["from"] && talk["from"] < to*3600 }
  end&.[]("organizer")
end

def format_hour(seconds)
  hours = seconds / 3600
  minutes = (seconds / 60) % 60

  "#{"%02d" % hours}:#{"%02d" % minutes}"
end

def logo(org)
  if File.exists?("img/apoios/#{org}.svg")
    "<img src='img/apoios/#{org}.svg' alt='#{org}' />"
  else
    "<img src='img/apoios/#{org}.png' alt='#{org}' />"
  end

end

def schedule_table(schedule, day)
  output = ""

  times = valid_times(schedule, day)

  lunch_started = {}

  organizer_times = [
    [15, 10, 12],
    [15, 14, 18],
    [16,  9, 12],
    [16, 14, 18],
    [17,  9, 12],
    [17, 14, 18],
  ]

  times.each do |(hour, minute)|
    seconds = (hour*60+minute)*60

    organizer_times.each do |(otd, ot_from, ot_to)|
      if [otd, ot_from, 0] == [day, hour, minute]
        output << "<tr><td></td>"
        schedule["rooms"].each do |room|
          org = organizer(schedule, day, room, ot_from, ot_to)
          if File.exists?("img/apoios/#{org}.svg")
            output << "<td><img src='img/apoios/#{org}.svg' alt='#{org}' /></td>"
          else
            output << "<td><img src='img/apoios/#{org}.png' alt='#{org}' /></td>"
          end
        end
        output << "</tr>"
      end
    end

    output << "<tr><td class='time' data-day='#{day}' data-seconds='#{seconds}'>#{hour.to_s.rjust(2,"0")}:#{minute.to_s.rjust(2,"0")}</td>\n"
    schedule["rooms"].each do |room|
      starting_talk = schedule["tracks"].select { |track| track["day"] == day && track["room"] == room }.flat_map { |track| track["schedule"] }.find { |talk| talk["from"] == seconds }
      during_talk = schedule["tracks"].select { |track| track["day"] == day && track["room"] == room }.flat_map { |track| track["schedule"] }.find { |talk| talk["from"] < seconds && seconds < talk["to"] }

      if starting_talk
        rows = rowspan(times, starting_talk["from"], starting_talk["to"])
        output << "<td class='busy' data-day='#{day}' data-seconds='#{starting_talk["from"]}' rowspan=#{rows}><a href='##{starting_talk["id"]}'>#{starting_talk["title"]}</a></td>\n"
      elsif during_talk
      elsif 12*3600 <= seconds && seconds < 14*3600
        if !lunch_started[room]
          rows = rowspan(times, seconds, 14*3600)
          slots = (14*3600 - seconds) / 900
          lunch_started[room] = true
          output << "<td class='break' rowspan='#{rows}'>Pausa de Almoço</td>\n"
        end
      elsif [day, hour, minute] == [15, 9, 45]
      elsif 19*3600 <= seconds && seconds < 20.5*3600
      else
        output << "<td></td>\n"
      end
    end

    if [day, hour, minute] == [15, 9, 45]
      output << "<td class='break' colspan='#{schedule["rooms"].size}' rowspan='1'>Arranque das salas das várias comunidades</td>\n"
    elsif seconds == 19*3600
      output << "<td class='break' colspan='#{schedule["rooms"].size}' rowspan='6'></td>\n"
    end

    output << "</tr>\n"
  end

  output
end

days = [15, 16, 17]
File.write("agenda.html", ERB.new(File.read("agenda.html.erb"), trim_mode: "-").result(binding))
